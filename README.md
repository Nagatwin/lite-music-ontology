# Report

This is a report for the lite music ontology made by Océane FOURQUET and Guillaume GOYON for the IA course. There are few individuals as example, one can add more if wanted.  
Categories are taken from : https://github.com/owlcs/miro/blob/master/miro.md

# A. Basics

## A.1 Ontology Name

Lite Music Ontology (LMO), v1.0

## A.2 Ontology Owner

Océane FOURQUET, Télécom SudParis, oceane.fourquet@telecom-sudparis.eu  
Guillaume GOYON, Télécom Paris, guillaume.goyon@telecom-paris.fr

## A.3 Ontology License

MIT

## A.4 Ontology URL

https://gitlab.com/Nagatwin/lite-music-ontology/blob/master/music.owl

## A.5 Ontology Repository

https://gitlab.com/Nagatwin/lite-music-ontology

## A.6 Methodological framework

We did not apply a common framework. 

However here is a summary of how this ontology was created:  
We first found a common theme for the members of the group, we thought about water and culture mainly.  
Then we did some research on existing ontologies and we started thinking about the basic classes of ours. Also we thought about what could this ontology be used in.

After what we made a quick overview of the classes, individuals and relations involved.

We gathered information on the web. Mainly wikipedia, the festivals' websites. We also used this website for a good basis for the instruments : http://www.les-instruments.com/classification.php

Then we build the ontology (classes, then object and data properties.

We finally classified it and used the reasoner to find and fix the issues.


# B. Motivation

## B.1 Need

This ontology is about music and can be used to resonate about periods, genre, artists, instruments or festivals.

## B.2 Competition

http://musicontology.com/ (MO) is a really complete ontology. However it is much heavier than this one.  
This one is meant to be make it easier to classify artists and song in different periods.

## B.3 Target audience

This ontology can be used by people that need to have a lite, yet precise, knowledge base about music.  

# C. Scope, requirements, development community

## C.1 Scope and coverage

This ontology gives basic knowledge about music, especially instruments, type of music, artists and songs. However, this ontology is not intended to teach solfeggio or music score reading.  
It can answer some questions such as :  
- When and where rock music appeared?  
- What are the principal instruments used in Reggae?

## C.2 Development community

Océane FOURQUET and Guillaume GOYON

## C.3 Communication

You can use the gitlab issues tab to communicate.  
https://gitlab.com/Nagatwin/lite-music-ontology/issues

# E. Ontology Content

## E.1 Knowledge Representation language

OWL version 2, EL profile.

## E.2 Development environment

Protégé, v5.5.0

## E.3 Ontology metrics

### Metrics

| Axiom | 410 |
|---|---|
| Logical axiom count | 203 |
| Declaration axioms count | 122 |
| Class count | 97 |
| Object property count | 10 |
| Data property count | 4 |
| Individual count | 12 |
| Annotation Property count | 2 |

### Class Axioms

| SubClassOf | 118 |
|---|---|
| EquivalentClasses | 3 |
| DisjointClasses | 27 |
| GCI count | 0 |
| Hidden GCI Count | 0 |

### Object Properties Axioms

| SubObjectPropertyOf | 1 |
|---|---|
| EquivalentObjectProperties | 0 |
| InverseObjectProperties | 2 |
| DisjointObjectProperties | 0 |
| FunctionalObjectProperty | 1 |
| InverseFunctionalObjectProperty | 1 |
| TransitiveObjectProperty | 0 |
| SymmetricObjectProperty | 0 |
| AsymmetricObjectProperty | 0 |
| ReflexiveObjectProperty | 0 |
| IrrefexiveObjectProperty | 0 |
| ObjectPropertyDomain | 9 |
| ObjectPropertyRange | 9 |
| SubPropertyChainOf | 0 |

### Data Proporty Axioms

| SubDataPropertyOf | 0 |
|---|---|
| FunctionalDataProperty | 4 |
| DataPropertyDomain | 4 |
| DataPropertyRange | 4 |

### Individual Axioms

| ClassAssertion | 7 |
|---|---|
| ObjectPropertyAssertion | 1 |
| DataPropertyAssertion | 12 |
| SameIndividual | 0 |

### Annotation Axioms

| AnnotationAssertion | 85 |
|---|---|
| AnnotationPropertyDomain | 0 |

## E.4 Incorporation of other ontologies

No other ontology imported.

## E.6 Identifier generation policy

CamelCase for entities and properties names. 

## E.7 Entity metadata policy

Each class has a short description

## E.8 Upper ontology

No upper ontology is used to match the requirements of the course.

## E.9 Ontology relationships

See figure 1 below

![Ontology_Relationship](./ontology.png){ width=100% }

| Object property | Description |
|---|---|
| isTheAuthorOf | An author of a song |
| hasBeenCreatedDuring | A song has been created during a period |
| isUsedIn | An instrument is used in a musical style |
| playsStyle | An author plays a certain style of music |
| belongsToStyle | A song belongs to a certain style |
| hasBeenCreatedIn | A style appeared in a location |
| hasBeenWrittenBy | A song has been written by an author |
| promoteStyle | A festival promotes a style |
| hasInstrument | A style uses some instruments |

| Object property | Func | Sym | InvFunc | Trans | Asym | Refl | Irrefl | Domain | Range | Inverse |
|---|---|---|---|---|---|---|---|---|---|---|
| isTheAuthorOf | x |  | x |  |  |  |  | Artist | Song | hasBeenWrittenBy |
| hasBeenCreatedDuring | x |  | x |  |  |  |  | Style | Period |  |
| isUsedIn |  |  |  |  |  |  |  | Instrument | Style | hasInstrument |
| playsStyle |  |  |  |  |  |  |  | Artist | Style |  |
| belongsToStyle | x |  |  |  |  |  |  | Song | Style |  |
| hasBeenCreatedIn |  |  |  |  |  |  |  | Style | Location |  |
| hasBeenWrittenBy | x |  | x |  |  |  |  | Song | Artist | isTheAuthorOf |
| promoteStyle |  |  |  |  |  |  |  | Festival | Style |  |
| hasInstrument |  |  |  |  |  |  |  | Style | Instrument | isUsedIn |


# F. Managing Change

## F.1 Sustainability plan

This ontology might be maintained using the git issues by the owners of the repository.

## F.2 Entity deprecation strategy

The owl:DeprecatedClass; no class is deleted from the ontology, but deprecated classes are labelled as obsolete with an annotation property.

## F.3 Versioning policy

The ontology is always provided with it's latest version on this repository. You can find older ones by going into the git history.

# G. Quality Assurance

## G.1 Testing

The HermiT (v1.4.3.456) reasoner has been used.

## G.2 Institutional endorsement

This ontology was made for the course IA301 of Telecom Paris.

# Example queries

### Songs that have a style parented to Afro Americans styles    

```
Song and belongsToStyle some AfroAmericanInfluence
```

This gives us the example of noWomanNoCry.  

We transformed it into a defined class AfroAmericanSong as an example.  

### Let's say you want to learn an instrument that is used in both rock and heavy metal. You can search :  


```
Instrument and isUsedIn some (HeavyMetal and Rock)
```

We get BassGuitar, ElectronicGuitar.


### Or maybe you want to check the styles creatated during the 20s:

```
Style and hasBeenCreatedDuring only 20's
```

We get Blues and Jazz

